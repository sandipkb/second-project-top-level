package com.sandipkb.secondproject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.sandipkb.secondproject.calculator.SimpleInterest;

/**
 * Unit test for simple App.
 */
public class CalculatorTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void checkSimpleInterest()
    {
    	assertEquals( SimpleInterest.interest(10000, 10, 5), 5000, .5 );
    }
}
