package com.sandipkb.secondproject.calculator;

/**
 * Simple Interest calculator
 * @author Sandip Bhattacharya
 *
 */
public class SimpleInterest {

	/**
	 * Calculate Simple Interest
	 * @param principal
	 * @param rate
	 * @param time
	 * @return
	 */
	public static double interest(double principal, double rate, double time){
		System.out.println("Calculating Simple Interest.....");
		return (principal*rate*time) / 100;
	}
}
